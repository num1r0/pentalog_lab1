Pentalog Lab. #1
================

Goal:
-----
Elaborate an application for `Users`, `Groups` and `Roles` management, by implementing following constraints:

- Users/Groups: many to one;
- Roles/Groups: many to many;

Advices:
--------
Application should be created with best practices in mind, like:

- Understandable names for attributes, classes, methods and objects;
- Use low coupling at the highest level;
- Read and use knowledge from provided sources (Java SE & JDBC Database Access);
