package datamappers;

import java.util.ArrayList;
import java.util.Arrays;

import com.sun.rowset.CachedRowSetImpl;

import models.Group;
import tscripts.SQLiteTransactionScript;

public class GroupsDataMapper {

	private static SQLiteTransactionScript sqliteTransactionScript;
	
	public GroupsDataMapper(){
		GroupsDataMapper.sqliteTransactionScript = new SQLiteTransactionScript();
	}
	
	@SuppressWarnings("finally")
	protected Group read(String groupId){
		String sqlStatement = "SELECT * FROM groups WHERE id=" + groupId + ";";
		Group group = null;
		try {
			CachedRowSetImpl rawObject = GroupsDataMapper.sqliteTransactionScript.read(sqlStatement);
			rawObject.first();
			group = new Group(
					new Integer(rawObject.getInt("id")),
					new String(rawObject.getString("name")),
					new ArrayList<String>(Arrays.asList(rawObject.getString("roles").split("|")))
			);
		} 
		catch (Exception ex) {
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println("User does not exist in the database. ErrorCode: " + ex.getClass().getName() + " " + ex.getMessage());
		}
		finally{
			return group;
		}
	}
	
	protected void insert(Group group){
		try{
			String sqlStatement = "INSERT INTO groups "
					+ "(id, name, roles) "
					+ "VALUES (" + group.getId() + ",'" + group.getName() + "','"
					+ group.getRoles() + "');";
			GroupsDataMapper.sqliteTransactionScript.create(sqlStatement);
		}
		catch(Exception ex){
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
		}
	}
	
	protected void update(Group group){
		try{
			String sqlStatement = "UPDATE groups SET "
					+ "name = '" + group.getName() + "', "
					+ "roles = '"	+ group.getRoles() + "', "
					+ " WHERE id = " + group.getId();
			GroupsDataMapper.sqliteTransactionScript.create(sqlStatement);
		}
		catch(Exception ex){
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
		}
	}
	
	protected void delete(Group group){
		try{
			String sqlStatement = "DELETE FROM groups WHERE id = " + group.getId();
			GroupsDataMapper.sqliteTransactionScript.delete(sqlStatement);
		}
		catch(Exception ex){
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println("Group does not exist in the database. ErrorCode: " + ex.getClass().getName() + " " + ex.getMessage());
		}
	}
}
