package datamappers;

import com.sun.rowset.CachedRowSetImpl;

import models.User;
import tscripts.SQLiteTransactionScript;

public class UsersDataMapper {
	private static SQLiteTransactionScript sqliteTransactionScript;
	
	public UsersDataMapper(){
		UsersDataMapper.sqliteTransactionScript = new SQLiteTransactionScript();
	}
	
	@SuppressWarnings("finally")
	protected User read(String userId){
		String sqlStatement = "SELECT * FROM users WHERE id=" + userId + ";";
		User user = null;
		try {
			CachedRowSetImpl rawObject = UsersDataMapper.sqliteTransactionScript.read(sqlStatement);
			rawObject.first();
			user = new User(
					new Integer(rawObject.getInt("id")),
					new String(rawObject.getString("username")),
					new String(rawObject.getString("password")),
					new String(rawObject.getString("fullname")),
					new String(rawObject.getString("gender")),
					new String(rawObject.getString("userGroup"))
			);
		} 
		catch (Exception ex) {
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println("User does not exist in the database. ErrorCode: " + ex.getClass().getName() + " " + ex.getMessage());
		}
		finally{
			return user;
		}
	}
	
	protected void insert(User user){
		try{
			String sqlStatement = "INSERT INTO users "
					+ "(id, username, password, fullname, gender, userGroup) "
					+ "VALUES (" + user.getId() + ",'" + user.getUsername() + "','"
					+ user.getPassword() + "','" + user.getFullName() + "','"
					+ user.getGender() + "','" + user.getGroup() + "');";
			UsersDataMapper.sqliteTransactionScript.create(sqlStatement);
		}
		catch(Exception ex){
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
		}
	}
	
	protected void update(User user){
		try{
			String sqlStatement = "UPDATE users SET "
					+ "username = '" + user.getUsername() + "', "
					+ "password = '"	+ user.getPassword() + "', "
					+ "fullname = '" + user.getFullName() + "', "
					+ "gender = '" + user.getGender() + "', "
					+ "userGroup = '" + user.getGroup() + "'"
					+ " WHERE id = " + user.getId() + ";";
			UsersDataMapper.sqliteTransactionScript.create(sqlStatement);
		}
		catch(Exception ex){
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
		}
	}
	
	protected void delete(User user){
		try{
			String sqlStatement = "DELETE FROM users WHERE id = " + user.getId();
			UsersDataMapper.sqliteTransactionScript.delete(sqlStatement);
		}
		catch(Exception ex){
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println("User does not exist in the database. ErrorCode: " + ex.getClass().getName() + " " + ex.getMessage());
		}
	}
}
