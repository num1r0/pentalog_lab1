package datamappers;

import com.sun.rowset.CachedRowSetImpl;

import models.Role;
import tscripts.SQLiteTransactionScript;

public class RolesDataMapper {
	private static SQLiteTransactionScript sqliteTransactionScript;
	
	public RolesDataMapper(){
		RolesDataMapper.sqliteTransactionScript = new SQLiteTransactionScript();
	}
	
	@SuppressWarnings("finally")
	protected Role read(String roleId){
		String sqlStatement = "SELECT * FROM roles WHERE id=" + roleId + ";";
		Role role = null;
		try {
			CachedRowSetImpl rawObject = RolesDataMapper.sqliteTransactionScript.read(sqlStatement);
			rawObject.first();
			boolean canWrite = false, canRead = false, canDelete = false;
			int id = rawObject.getInt("id");
			String name = rawObject.getString("name");
			int can_write = rawObject.getInt("can_write");
			int can_read = rawObject.getInt("can_read");
			int can_delete = rawObject.getInt("can_delete");
			if (can_write > 0) canWrite = true;
			if (can_read > 0) canRead = true;
			if (can_delete > 0) canDelete = true;
			role = new Role(new Integer(id), name, canWrite, canRead, canDelete);
		} 
		catch (Exception ex) {
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println("User does not exist in the database. ErrorCode: " + ex.getClass().getName() + " " + ex.getMessage());
		}
		finally{
			return role;
		}
	}
	
	protected void insert(Role role){
		try{
			String sqlStatement = "INSERT INTO roles "
					+ "(id, name, can_write, can_read, can_delete) "
					+ "VALUES (" + role.getId() + ",'" + role.getName() + "','"
					+ (role.canWrite() ? 1 : 0) + "','" + (role.canRead() ? 1 : 0) + "','"
					+ (role.canDelete() ? 1 : 0) + "');";
			RolesDataMapper.sqliteTransactionScript.create(sqlStatement);
		}
		catch(Exception ex){
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
		}
	}
	
	protected void update(Role role){
		try{
			String sqlStatement = "UPDATE roles SET "
					+ "name = '" + role.getName() + "', "
					+ "can_write = '"	+ (role.canWrite() ? 1 : 0) + "', "
					+ "can_read = '" + (role.canRead() ? 1 : 0) + "', "
					+ "can_delete = '" + (role.canDelete() ? 1 : 0) + "', "
					+ " WHERE id = " + role.getId() + ";";
			RolesDataMapper.sqliteTransactionScript.create(sqlStatement);
		}
		catch(Exception ex){
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
		}
	}
	
	protected void delete(Role role){
		try{
			String sqlStatement = "DELETE FROM roles WHERE id = " + role.getId();
			RolesDataMapper.sqliteTransactionScript.delete(sqlStatement);
		}
		catch(Exception ex){
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println("User does not exist in the database. ErrorCode: " + ex.getClass().getName() + " " + ex.getMessage());
		}
	}
}
