package tscripts;

import java.sql.*;

import com.sun.rowset.CachedRowSetImpl;

public class SQLiteTransactionScript implements ITransactionScript {
	/*
	 * Transaction script which implements ITransactionScript interface.
	 * This class is a specific implementation for Application -> SQLite 
	 * database transactions
	 */
	
	/* ####### NOTE #######
	 * 
	 * I think it is possible to create a single Connection object per 
	 * SQLiteTransactionScript object, but implement something like queue of
	 * transactions to perform.
	 * In this way, we'll minimize the number of connections to the database,
	 * which (from my experience of Systems Administrator) is a good practice.
	 * 
	 * But in this case, I'll create a connection per transaction.
	 * 
	 * ####################
	 */
	
	public void create(String sqlStatement){
		try{
			Class.forName("org.sqlite.JDBC");
			Connection databaseConnection = DriverManager.getConnection("jdbc:sqlite:database");
			databaseConnection.setAutoCommit(false);
			Statement statement = databaseConnection.createStatement();
			statement.executeUpdate(sqlStatement);
			statement.close();
			databaseConnection.commit();
			databaseConnection.close();
		}
		catch (Exception ex){
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
		}
	}
	
	@SuppressWarnings({ "finally", "resource" })
	public CachedRowSetImpl read(String sqlStatement){
		CachedRowSetImpl result = null;
		
		try{
			Class.forName("org.sqlite.JDBC");
			Connection databaseConnection = DriverManager.getConnection("jdbc:sqlite:database");
			databaseConnection.setAutoCommit(false);
			Statement statement = databaseConnection.createStatement();
			ResultSet queryResult = statement.executeQuery(sqlStatement);
			result = new CachedRowSetImpl();
			result.populate(queryResult);
			statement.close();
			databaseConnection.commit();
			databaseConnection.close();
		}
		catch (Exception ex){
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
		}
		finally{
			return result;
		}
	}
	
	public void update(String sqlStatement){
		try{
			Class.forName("org.sqlite.JDBC");
			Connection databaseConnection = DriverManager.getConnection("jdbc:sqlite:database");
			databaseConnection.setAutoCommit(false);
			Statement statement = databaseConnection.createStatement();
			statement.executeUpdate(sqlStatement);
			statement.close();
			databaseConnection.commit();
			databaseConnection.close();
		}
		catch (Exception ex){
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
		}
	}
	
	public void delete(String sqlStatement){
		try{
			Class.forName("org.sqlite.JDBC");
			Connection databaseConnection = DriverManager.getConnection("jdbc:sqlite:database");
			databaseConnection.setAutoCommit(false);
			Statement statement = databaseConnection.createStatement();
			statement.executeUpdate(sqlStatement);
			statement.close();
			databaseConnection.commit();
			databaseConnection.close();
		}
		catch (Exception ex){
			// We can send this exception to a Logging class
			// Right now, just print in console
			System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
		}
	}
}
