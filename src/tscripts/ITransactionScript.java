package tscripts;

public interface ITransactionScript{
	/*
	 * Interface implemented by all transaction scripts
	 * in this application.
	 * It describes basic CRUD functionality
	 */
	
	public void create(String statement);
	public Object read(String statement);
	public void update(String statement);
	public void delete(String statement);
}
