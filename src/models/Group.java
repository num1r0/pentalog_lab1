package models;

import java.util.ArrayList;
import java.util.List;

import datamappers.GroupsDataMapper;

public class Group extends GroupsDataMapper{
	private Integer id; // Default group for guest users
	private String name;
	private List<String> roles;
	
	// Custom constructors
	public Group(){
		this.id = null;
		this.name = null;
		this.roles = new ArrayList<String>();
	}
	
	public Group(Integer groupId, String groupName, List<String> groupRoles){
		this.id = groupId;
		this.name = groupName;
		this.roles = groupRoles;
	}
	
	// Setters
	public void setId(Integer groupId){
		this.id = groupId;
	}
	public void setName(String groupName){
		this.name = groupName;
	}
	public void setRoles(List<String> groupRoles){
		this.roles = groupRoles;
	}
	
	// Getters
	public int getId(){
		return this.id.intValue();
	}
	public String getName(){
		return this.name;
	}
	public List<String> getRoles(){
		return this.roles;
	}
	
	// Model specific methods, like saving, creating and removing the group
	public void load(String id){
		Group group = super.read(id);
		if(null != group)
			copyGroup(group);
	}
	public void save(){
		super.update(this);
	}
	public void add(){
		super.insert(this);
	}
	public void remove(){
		super.delete(this);
	}
	
	private void copyGroup(Group group){
		this.id = group.getId();
		this.name = group.getName();
		this.roles = group.getRoles();
	}
}
