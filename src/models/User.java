package models;

import java.util.Random;

import datamappers.UsersDataMapper;

public class User extends UsersDataMapper{
	/*
	 * User Model class. In this specific case, a user is defined
	 * by a unique Id, username, password, full name, gender and group
	 */
	private Integer id;
	private String username;
	private String password;
	private String fullName;
	private String gender;
	private String groupName;

	// Custom constructors
	public User(){
		this.id = null;
		this.username = null;
		this.password = null;
		this.fullName = null;
		this.gender = null;
		this.groupName = null;
	}
	public User(int id, String username, String password){
		this.id = new Integer(id);
		this.username = username;
		this.password = password;
		this.fullName = null;
		this.gender = null;
		this.groupName = null;
	}
	
	public User(int id, String username, String password, String fullName, String gender){
		this.id = new Integer(id);
		this.username = username;
		this.password = password;
		this.fullName = fullName;
		this.gender = gender;
		this.groupName = null;
	}
	
	public User(int id, String username, String password, 
			String fullName, String gender, String groupName){
		this.id = new Integer(id);
		this.username = username;
		this.password = password;
		this.fullName = fullName;
		this.gender = gender;
		this.groupName = groupName;
	}
	public User(String username, String password, 
			String fullName, String gender, String groupName){
		Random pseudoRandom = new Random();
		this.id = new Integer(pseudoRandom.nextInt(9999));
		this.username = username;
		this.password = password;
		this.fullName = fullName;
		this.gender = gender;
		this.groupName = groupName;
	}
	
	// Setters
	public void setId(int id){ 
		this.id = id; 
	}
	public void setUsername(String username){
		this.username = username;
	}
	public void setPassword(String password){
		this.password = password;
	}
	public void setFullName(String fullName){
		this.fullName = fullName;
	}
	public void setGender(String gender){
		this.gender = gender;
	}
	public void setGroup(String groupName){
		this.groupName = groupName;
	}
	
	// Getters
	public Integer getId(){
		return this.id.intValue();
	}
	public String getUsername(){
		return this.username;
	}
	public String getPassword(){
		return this.password;
	}
	public String getFullName(){
		return this.fullName;
	}
	public String getGender(){
		return this.gender;
	}
	public String getGroup(){
		return this.groupName;
	}
	
	// Model specific methods, like saving, creating and removing the user
	public void load(String id){
		User user = super.read(id);
		if(null != user)
			copyUser(user);
	}
	public void save(){
		super.update(this);
	}
	public void add(){
		super.insert(this);
	}
	public void remove(){
		super.delete(this);
	}
	
	private void copyUser(User user){
		this.id = user.id;
		this.username = user.username;
		this.password = user.password;
		this.fullName = user.fullName;
		this.gender = user.gender;
		this.groupName = user.groupName;
	}
}
