package models;

import datamappers.RolesDataMapper;

public class Role extends RolesDataMapper{
	/*
	 * Role Model class. I chose a particular case where
	 * a role has: Id, name, write, read, delete permissions
	 */
	private Integer id;
	private String name;
	private boolean canWrite;
	private boolean canRead;
	private boolean canDelete;
	
	// Custom constructors
	public Role(){
		this.id = null;
		this.name = null;
		this.canWrite = false;
		this.canRead = false;
		this.canDelete = false;
	}
	public Role(Integer roleId, String roleName, boolean canWrite,
				boolean canRead, boolean canDelete){
		this.id = roleId;
		this.name = roleName;
		this.canWrite = canWrite;
		this.canRead = canRead;
		this.canDelete = canDelete;
	}
	
	// Setters
	public void setId(int roleId){
		this.id = roleId;
	}
	public void setName(String roleName){
		this.name = roleName;
	}
	public void setCanWrite(boolean canWrite){
		this.canWrite = canWrite;
	}
	public void setCanRead(boolean canRead){
		this.canRead = canRead;
	}
	public void setCanDelete(boolean canDelete){
		this.canDelete = canDelete;
	}
	
	// Getters
	public int getId(){
		return this.id;
	}
	public String getName(){
		return this.name;
	}
	public boolean canWrite(){
		return this.canWrite;
	}
	public boolean canRead(){
		return this.canRead;
	}
	public boolean canDelete(){
		return this.canDelete;
	}
	
	// Model specific methods, like saving, creating and removing the user
	public void load(String id){
		Role role = super.read(id);
		if(null != role)
			copyRole(role);
	}
	public void save(){
		super.update(this);
	}
	public void add(){
		super.insert(this);
	}
	public void remove(){
		super.delete(this);
	}
	
	private void copyRole(Role role){
		this.id = role.id;
		this.name = role.name;
		this.canWrite = role.canWrite;
		this.canRead = role.canRead;
		this.canDelete = role.canDelete;
	}
}
